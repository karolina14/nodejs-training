//Old modules
// const action = () => {
//     console.log('hello');
// }
//
// module.exports = action

//ES6
export const action = () => {
    console.log('hello');
}